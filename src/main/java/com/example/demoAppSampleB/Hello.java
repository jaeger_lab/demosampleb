package com.example.demoAppSampleB;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
class Hello {

    @Autowired
    RestTemplate restTemplate;
    
    @RequestMapping("/helloB")
    public String hello() {
        return "Hello app B";
    }

   @RequestMapping(value="/chainingtoa")
    public String chainingToA() {
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:8081/people/1", String.class);
        return response.getBody();
    }

    @RequestMapping("/lastchaining")
    public String lastChaining() {
        ResponseEntity<String> response = restTemplate.getForEntity("http://google.com", String.class);
        return "Chaining : " + response.getBody();
    }

    @RequestMapping("/timeout")
    public String timeOut( )throws Exception {
        Thread.sleep(10000000);
        return "Timeout : 10s ";
    }

}
